# OpenML dataset: Flare

https://www.openml.org/d/46176

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**flare** dataset from the **KEEL** repository: https://sci2s.ugr.es/keel/category.php?cat=clas

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46176) of an [OpenML dataset](https://www.openml.org/d/46176). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46176/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46176/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46176/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

